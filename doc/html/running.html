<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>Running a SLUG simulation &mdash; slug 2.0 documentation</title>
    
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '2.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="top" title="slug 2.0 documentation" href="index.html" />
    <link rel="next" title="Parameter Specification" href="parameters.html" />
    <link rel="prev" title="Compiling and Installing SLUG" href="compiling.html" />
   
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9">

  </head>
  <body role="document">
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="parameters.html" title="Parameter Specification"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="compiling.html" title="Compiling and Installing SLUG"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">slug 2.0 documentation</a> &raquo;</li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="running-a-slug-simulation">
<h1>Running a SLUG simulation<a class="headerlink" href="#running-a-slug-simulation" title="Permalink to this headline">¶</a></h1>
<p>Once SLUG is compiled, running a simulation is extremely simple. The first step, which is not required but makes life a lot simpler, is to set the environment variable <code class="docutils literal"><span class="pre">SLUG_DIR</span></code> to the directory where you have installed SLUG. If you are using a <code class="docutils literal"><span class="pre">bash</span></code>-like shell, the syntax for this is:</p>
<div class="highlight-rest"><div class="highlight"><pre>export SLUG_DIR = /path/to/slug
</pre></div>
</div>
<p>while for a <code class="docutils literal"><span class="pre">csh</span></code>-like shell, it is:</p>
<div class="highlight-rest"><div class="highlight"><pre>setenv SLUG_DIR /path/to/slug
</pre></div>
</div>
<p>This is helpful because SLUG needs a lot of input data, and if you don&#8217;t set this variable, you will have to manually specify where to find it.</p>
<p>Next, to run on a single processor, just do:</p>
<div class="highlight-rest"><div class="highlight"><pre>./bin/slug param/filename.param
</pre></div>
</div>
<p>where <code class="docutils literal"><span class="pre">filename.param</span></code> is the name of a parameter file, formatted as specified in <a class="reference internal" href="parameters.html#sec-parameters"><span>Parameter Specification</span></a>. The code will write a series of output files as described in <a class="reference internal" href="output.html#sec-output"><span>Output Files and Format</span></a>.</p>
<p>If you have more than one core at your disposal, you can also run SLUG in parallel, using the command line:</p>
<div class="highlight-rest"><div class="highlight"><pre>python ./bin/slug.py param/filename.param
</pre></div>
</div>
<p>This called a python script that automatically divides up the Monte Carlo trials you have requested between the available processors, then consolidates the output so that it looks the same as if you had run a single-processor job. The python script allows fairly fine-grained control of the parallelism. It accepts the following command line arguments:</p>
<ul class="simple">
<li><code class="docutils literal"><span class="pre">-n</span> <span class="pre">NPROC,</span> <span class="pre">--nproc</span> <span class="pre">NPROC</span></code>: this parameter specifies the number of simultaneous SLUG processes to run. It defaults to the number of cores present on the machine where the code is running</li>
<li><code class="docutils literal"><span class="pre">-b</span> <span class="pre">BATCHSIZE,</span> <span class="pre">--batchsize</span> <span class="pre">BATCHSIZE</span></code>: this specifies how to many trials to do per SLUG process. It defaults to the total number of trials requested divided by the total number of processes, rounded up, so that only one SLUG process is run per processor. <em>Rationale</em>: The default behavior is optimal from the standpoint of minimizing the overhead associated with reading data from disk, etc. However, if you are doing a very large number of runs that are going to require hours, days, or weeks to complete, and you probably want the code to checkpoint along the way. In that case it is probably wise to set this to a value smaller than the default in order to force output to be dumped periodically.</li>
<li><code class="docutils literal"><span class="pre">-nc,</span> <span class="pre">--noconsolidate</span></code>: by default the <code class="docutils literal"><span class="pre">slug.py</span></code> script will take all the outputs produced by the parallel runs and consolidate them into single output files, matching what would have been produced had the code been run in serial mode. If set, this flag suppresses that behavior, and instead leaves the output as a series of files whose root names match the model name given in the parameter file, plus the extension <code class="docutils literal"><span class="pre">_pPPPPP_nNNNNN</span></code>, where the digits <code class="docutils literal"><span class="pre">PPPPP</span></code> give the number of the processor that produces that file, and the digits <code class="docutils literal"><span class="pre">NNNNN</span></code> give the run number on that processor. <em>Rationale</em>: normally consolidation is convenient. However, if the output is very large, this may produce undesirably bulky files. Furthermore, if one is doing a very large number of simulations over an extended period, and the <code class="docutils literal"><span class="pre">slug.py</span></code> script is going to be run multiple times (e.g.due to wall clock limits on a cluster), it may be preferable to leave the files unconsolidated until all runs have been completed.</li>
</ul>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h4>Previous topic</h4>
  <p class="topless"><a href="compiling.html"
                        title="previous chapter">Compiling and Installing SLUG</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="parameters.html"
                        title="next chapter">Parameter Specification</a></p>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/running.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3>Quick search</h3>
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
      <input type="hidden" name="check_keywords" value="yes" />
      <input type="hidden" name="area" value="default" />
    </form>
    <p class="searchtip" style="font-size: 90%">
    Enter search terms or a module, class or function name.
    </p>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2014, Mark Krumholz, Michele Fumagalli, et al..
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 1.3.1</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.2</a>
      
      |
      <a href="_sources/running.txt"
          rel="nofollow">Page source</a></li>
    </div>

    

    
  </body>
</html>